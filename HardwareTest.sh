#!/bin/bash

num=$(ls /dev | grep 'sr-imu' | wc -m)

if (( $num>0 )); then
	echo ' IMU Board:		[OK]'
else
	echo 'IMU Board:		[FAIL]'
fi


num=$(ls /dev | grep 'sr-lightboard'| wc -m)

if (( $num>0 )); then
	echo ' Light Board:		[OK]'
else
	echo 'Light Board:		[FAIL]'
fi

num=$(ls /dev | grep 'sr-motorboard'| wc -m)

if (( $num>0 )); then
	echo ' Motor Board:		[OK]'
else
	echo 'Motor Board:		[FAIL]'
fi

num=$(ls /dev | grep 'sr-sensorboard'| wc -m)

if (( $num>0 )); then
	echo ' Sensor Board:		[OK]'
else
	echo 'Sensor Board:		[FAIL]'
fi

num=$(ls /dev | grep 'sr-touchscreen'| wc -m)

if (( $num>0 )); then
	echo ' Touchscreen:		[OK]'
else
	echo 'Touchscreen:		[FAIL]'
fi


num=$(lsusb | grep '2bc5:0402'| wc -m)

if (( $num>0 )); then
	echo 'Orbbec Sensor:		[OK]'
else
	echo 'Orbbec Sensor:		[Fail]'
fi

num=$(lsusb | grep 'LifeCam Studio'| wc -l)
echo "HD Cameras Count:	[$num]"


