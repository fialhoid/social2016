##Monarch 2wd Robot ROS packages

IDmind's Monarch robot drivers and functionalities.

Originally developed by João Freite, <jfreire@idmind.pt>

Modified and Maintained by David Portugal, <davidbsp@citard-serv.com>

```
$ monarch_2dnav
$ monarch_drivers
$ monarch_teleop
$ monarch_description
```
##########################################################################
# 
# IDmind - Readme file 02-2016
# Modifications made by Nuno Fialho <nfialho@idmind.pt>
# 
##########################################################################

# Verify hardware availability

run: ./HardwareTest.sh

#Initialize catkin_paths

run: source ~/catkin_social/devel/setup.bash



#Social Robots with Hokuyo over ethernet

run: roslaunch monarch_2dnav office_HokuyoLan.launch

#Social Robots with Hokuyo over USB

run: roslaunch monarch_2dnav office.launch



# IDmind touchscreen middle driver

run: sudo ~/catkin_social/src/social_robot/IDmind_touchdriver/startidmind_touch.sh

