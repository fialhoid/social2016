#include <ros/ros.h>
#include <sensor_msgs/Range.h>
#include <sensor_msgs/PointCloud.h>
#include <tf/transform_listener.h>

/** IMPORTANTE NOTE: THIS SOURCE CODE ASSUMES THAT THERE EXISTS STATIC TFs BEING PUBLSHED CONTINUOSLY BETWEEN "base_link" and "sonar_X" **/

ros::Publisher *sonars_pcl_pub;
sensor_msgs::PointCloud sonar_pcl;
double sonars[12];
double max_sonar_range, min_sonar_range;


//Sonars Callback -- Fill sonars[12]:
void SonarsReceived(const sensor_msgs::Range::ConstPtr& sonar){

    std::string frame_id = sonar->header.frame_id;
    std::string sonar_id = frame_id.substr (7,2);		//get sonar id from frame_id
    //ROS_INFO("sonar_id (str): %s", sonar_id.c_str());
      
    int id = atoi(sonar_id.c_str());
    //ROS_INFO("sonar_id (int): %d", id);
    
    sonars[id] = sonar->range;
    max_sonar_range = sonar->max_range;
    min_sonar_range = sonar->min_range;
}


void publish_backsonars_pcl (tf::TransformListener &TFListenerSonarPCL, std::string base_frame_id){ //only back sonar (sonar 6)

  char frame_id[10];  
  
  sonar_pcl.header.stamp = ros::Time::now();
  sonar_pcl.header.frame_id = base_frame_id;
  
  //Points to Transform from /sonar_x to /base_link:
  geometry_msgs::PointStamped sonar_point, base_point;
  sonar_point.header.stamp = ros::Time();	 	//use the most recent transform available
  sonar_point.point.y = sonar_point.point.z = 0.0;
  geometry_msgs::Point32 cloud_pt;
  
  for(int i=6; i<=6; i++){	//for all sonars you would run: for(int i=0; i<12; i++)
    
     sprintf(frame_id, "/sonar_%d", i);   
     
     sonar_point.header.frame_id = frame_id;
     sonar_point.point.x = sonars[i];			//range
     
     //ROS_FATAL("sonar %d",i);
     //ROS_FATAL("max_sonar_range: %f", max_sonar_range);
     //ROS_FATAL("min_sonar_range: %f", min_sonar_range);
     //ROS_FATAL("range: %f", sonars[i]);
    
   // if(sonars[i]<=max_sonar_range && sonars[i]>=min_sonar_range){
      try{
	TFListenerSonarPCL.waitForTransform(base_frame_id, frame_id, sonar_pcl.header.stamp, ros::Duration(0.1));
	TFListenerSonarPCL.transformPoint(base_frame_id, sonar_point, base_point);
	
      }catch (tf::TransformException &ex){
	  ROS_ERROR("Could not transform point from \"%s\" to \"%s\". Exception: %s", frame_id,base_frame_id.c_str(),ex.what());  
	  break;
      } 
    
    
    //build the pcl:
     cloud_pt.x = base_point.point.x;
     cloud_pt.y = base_point.point.y;
     cloud_pt.z = base_point.point.z;
     sonar_pcl.points.push_back(cloud_pt);   
  }
  
  //Publish Cloud:
  sonars_pcl_pub->publish(sonar_pcl);
  sonar_pcl.points.clear();  
}


int main(int argc, char** argv){
  
  ros::init(argc, argv, "sonar2pcl");
  ros::NodeHandle pn("~");

  std::string base_frame_id;
  
  //in the social robot case, the "base_link" frame is not in the center of the sonar rings, so we'll pass another frame_id (e.g., "backsonars_center") as the physical center of the robot.
  pn.param<std::string>("base_frame_id", base_frame_id, "base_link");
  
  tf::TransformListener TFListenerSonarPCL;
  
  ros::Publisher pub_pcl_s = pn.advertise<sensor_msgs::PointCloud>("/backsonar_pcl", 50);
  sonars_pcl_pub = &pub_pcl_s;  
  
  ros::Subscriber sonar_sub  = pn.subscribe<sensor_msgs::Range>("/sonars", 50, SonarsReceived);  
  
  ros::Duration(1.0).sleep(); //Let Tf buffer fill
  ROS_INFO("Publishing Sonar Point Cloud...");
  
  ros::Rate r(10.0);
		
  while(pn.ok()){
    publish_backsonars_pcl(TFListenerSonarPCL,base_frame_id);
    ros::spinOnce();
    r.sleep();
  }
  
  return 0;
}