#!/usr/bin/env python
#

import rospy
from monarch_drivers.msg import BatteryState
import Tkinter as tk
from Tkinter import *
from ttk import *
import signal


bat_motor = 0.0
bat_elec = 0.0
bat_cable = 0.0
bat_pc = 0.0


def signal_handler(signal, frame):	# Handle Ctrl+C
  sys.exit()
        
         
def callback(data):
  #rospy.loginfo("I heard %f",data.bat_elec)
  global bat_motor
  global bat_elec
  global bat_cable
  global bat_pc
  bat_motor = data.bat_motor
  bat_elec = data.bat_elec
  bat_cable = data.bat_cable  
  bat_pc = data.bat_pc
  

class App(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)
        self.title("SR Battery Status")        
        self.geometry("250x125+0+0")
        self.style = Style()
        self.style.theme_use("alt")
        self.resizable(width=FALSE, height=FALSE)
        #self.pack(fill=BOTH, expand=1)      
        self.protocol('WM_DELETE_WINDOW', self.on_exit)
        self.initUI()
        self.updateUI()
        
        
    def initUI(self):
       
        #self.columnconfigure(1, weight=1)
        #self.columnconfigure(3, pad=7)
        #self.rowconfigure(3, weight=1)
        #self.rowconfigure(7, pad=7)
        
        lbl = Label(self, text="Motor Battery")
        lbl.grid(sticky=W, row = 0, column= 0, pady=4, padx=5)
        lbl = Label(self, text="Electronics Battery")
        lbl.grid(sticky=W, row = 1, column= 0, pady=4, padx=5)
        lbl = Label(self, text="Cable Battery")
        lbl.grid(sticky=W, row = 2, column= 0, pady=4, padx=5)
        lbl = Label(self, text="PC Battery")
        lbl.grid(sticky=W, row = 3, column= 0, pady=4, padx=5)


    def updateUI(self):
        lbl = Label(self, text="%.1f V" % bat_motor)			#PUT VALUE HERE
        lbl.grid(sticky=W, row = 0, column= 1, pady=4, padx=5)
        lbl = Label(self, text="%.1f V" % bat_elec)			#PUT VALUE HERE
        lbl.grid(sticky=W, row = 1, column= 1, pady=4, padx=5)    
        lbl = Label(self, text="%.1f V" % bat_cable)			#PUT VALUE HERE
        lbl.grid(sticky=W, row = 2, column= 1, pady=4, padx=5)                
        lbl = Label(self, text="%.1f V" % bat_pc)			#PUT VALUE HERE
        lbl.grid(sticky=W, row = 3, column= 1, pady=4, padx=5)    
        self.after(2500, self.updateUI)				#CALL updateUI every 2.5 secs
      

    def on_exit(self):
	self.destroy()
	sys.exit()



def main():
  
  rospy.init_node('node_name')
  rospy.Subscriber("battery_state", BatteryState, callback)
  
  signal.signal(signal.SIGINT, signal_handler)
  
  App().mainloop()
    
  # spin() simply keeps python from exiting until this node is stopped
  rospy.spin()

if __name__ == '__main__':
    main()