#!/bin/bash
ids=$(xinput --list |grep 'Touch Fusion'| awk -v search="$SEARCH" \
    '$0 ~ search {match($0, /id=[0-20]+/);\
                  if (RSTART) \
                    print substr($0, RSTART+3, RLENGTH-3)\
                 }'\
     )
xinput disable $ids
